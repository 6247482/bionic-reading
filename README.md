# Bionic Reading
In this repository you can find all the materials used in the paper "Does Bionic Reading® actually work?"

## Data analysis
The data analysis has been performed partially in `R` and `SPSS`.

## Survey
The full qualtrics survey can be accessed as a `.qsf` file as well as a `.pdf`. Individual texts and used resources are also available seperately.

## Online Materials
Below you can find links to several online resources that have been used:
- [UU Qualtrics](https://survey.uu.nl/)
- [Bionic Reading® Text Generator](https://app.bionic-reading.com/)